package com.avast;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ahsan on 4/7/15.
 */
public class Deck {

    List<RedCard> redDeck;
    List<BlackCard> blackDeck;

    public Deck() {
        redDeck = new LinkedList<RedCard>();
        blackDeck = new LinkedList<BlackCard>();
        redDeck.add(new RedCard());
        blackDeck.add(new BlackCard());
    }

    public static void main(String[] args) {

        /* Two types of iterators */
        Deck D = new Deck();
        // Can you add to deck here?
        for (RedCard R : D.redDeck) {
            System.out.println(R);
        }
        for (BlackCard B : D.blackDeck) {
            System.out.println(B);
        }
    }

    /* Overload function add() */
    public void add(RedCard card) {
        redDeck.add(card);
    }

    public void add(BlackCard card) {
        blackDeck.add(card);
    }

    private class RedCard implements Iterable<RedCard> {

        @Override
        public Iterator<RedCard> iterator() {
            return redDeck.iterator();
        }

        @Override
        public String toString() {
            return "RedCard{}";
        }
    }

    private class BlackCard implements Iterable<BlackCard> {

        @Override
        public Iterator<BlackCard> iterator() {
            return blackDeck.iterator();
        }

        @Override
        public String toString() {
            return "BlackCard{}";
        }
    }
}