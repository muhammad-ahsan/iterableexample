package com.avast;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ahsan on 4/7/15.
 */

// Multiple iterators for a custom collection
public class Line implements Iterable<Point> {
    // Line is collection of Point
    List<Point> pointList = new LinkedList<Point>();

    public void add(Point p) {
        pointList.add(p);
    }

    @Override
    public Iterator<Point> iterator() {
        return pointList.iterator();
    }

}
