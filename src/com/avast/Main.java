package com.avast;

import java.util.Iterator;

public class Main {


    public static void main(String[] args) {

        Line L = new Line();
        L.add(new Point(1, 1));
        L.add(new Point(2, 2));

        Iterator<Point> I = L.iterator();
        while (I.hasNext()) {

            Point P = I.next();
            System.out.println(P.x + "," + P.y);

        }
        System.out.println("*********************************");
        // Client code don't know internal storage of Line
        for (Point P : L) {
            System.out.println(P.x + "," + P.y);
        }

    }
}
